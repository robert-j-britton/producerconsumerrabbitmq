﻿using RabbitMQ.Client;
using System;
using System.Text;
using Newtonsoft.Json;

namespace Producer
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int16 Age { get; set; }

    }


    class Sender
    {
        static void Main(string[] args)
        {
            var customer = new Customer
            {
                FirstName = "Robert",
                LastName = "Britton",
                Age = 31
            };

            var factory = new ConnectionFactory { HostName = "localhost" };
            using(var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare("BasicTest", false, false, false, null);

                string message = JsonConvert.SerializeObject(customer);
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish("", "BasicTest", null, body);
                Console.WriteLine("Sent message {0}", message);
            }

            Console.WriteLine("Press [enter] to exit the Sender App...");
            Console.ReadLine();
        }
    }
}
